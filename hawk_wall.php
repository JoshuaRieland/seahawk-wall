<?php

SESSION_START();
require_once("new-connection.php");

$query = "SELECT users.first_name, users.last_name, messages.created_at, messages.content, messages.id AS msgID, messages.user_id, comments.id, comments.user_id, comments.message_id FROM users INNER JOIN messages ON users.id = messages.user_id LEFT JOIN comments ON messages.id = comments.message_id GROUP BY messages.id ORDER BY messages.created_at DESC";
$history = fetch_all($query);
$query2 = "SELECT comments.content, comments.message_id, comments.user_id, comments.created_at ,users.first_name, users.last_name FROM comments LEFT JOIN users ON comments.user_id = users.id";
$history2 = fetch_all($query2);

?>

<html>
<head>
	<title>The Hawk Wall</title>
	<link rel="stylesheet" type="text/css" href="style.wall.css">
</head>
<body class="blackBack">
	<div class="wall_container">
		<form method = "POST" name="logout" action="process.php">
		<p class="logoutLink"><input type="submit" name="logout" class="logout" value="Log Out"></p>
		<input type="hidden" name="hidden_logout">
		</form>
		<h2 class="helloText">Hello <?=$_SESSION['first_name']?> <?=$_SESSION['last_name']?></h2>
		<div class="wall_error_box">
<?php
			if(isset($_SESSION['null_message']))
			{
				for($i = 0; $i < count($_SESSION['null_message']); $i++)
				{
?>
				<p class="errorText"><?=$_SESSION['null_message'][$i]?></p>
<?php
				}
				unset($_SESSION['null_message']);
			}
?>
		</div><!-- END: wall_error_box -->
		<div class="message_box">
			<form method="POST" action="process.php">
				<p>Share your love for the Seahawks:</p>			
				<input type="hidden" name="hidden_message">
				<input type="textarea" name="message" class="message_area">
				<p>Share Post!</p>
				<input type="submit" name="message_btn" class="message_btn" value="">
			</form>	
		</div><!-- END: message_box -->
	</div><!-- END: wall_container -->
	<div class="history_container">

		<div class="error_center">
<?php
			if(isset($_SESSION['null_message2']))
			{
				for($i = 0; $i < count($_SESSION['null_message2']); $i++)
				{
?>
				<p> <?=$_SESSION['null_message2'][$i]?> </p>
<?php
				}
				unset($_SESSION['null_message2']);
			}
?>
		</div><!-- END: error_center -->
		
		<div class="message_center">

			
<?php
				for($i = 0; $i < count($history); $i++)
				{
					if(!empty($history[$i]['content']))
					{
						// var_dump($history[$i]['content']);
?>					<div class="message_post">
						<p class="message_author">Author: <?=$history[$i]['first_name']?> <?=$history[$i]['last_name'];?></p>
						<p class="message_time"><?=$history[$i]['created_at']?></p>
						<p class="message_content"><?=$history[$i]['content']?></p>
<!----><?php
							for($j = 0; $j < count($history2) ; $j++)
							{
								if($history[$i]['message_id'] == $history2[$j]['message_id'])
								{
?>
									<div class="comment_post">
										<p class="comment_author">Author: <?=$history2[$j]['first_name']?></p>
										<p class="comment_time"><?=$history2[$j]['created_at']?></p>
										<p class="comment_content"> <?=$history2[$j]['content']?></p>
									</div><!--comment-post-->
<?php
								}
							}					


?><!---->
						<form method="POST" action="process.php">
							<input type="hidden" name="hidden_comment" value="<?= $history[$i]['msgID'] ?>">
							<input type="textarea" name="comment_text" class="comment_area" placeholder="Comment Here">
							<input type="submit" name="comment_btn" class="comment_btn" value="">
						</form>
					</div><!-- END: message_post -->
<?php
					}
				}

?>
		</div><!-- END: message_center -->
	</div>
</body>
</html>