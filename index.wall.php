<?php
SESSION_START();
require_once("new-connection.php");
?>

<html>
<head>
	<title>The Wall</title>
	<link rel="stylesheet" type="text/css" href="style.wall.css">
</head>
<body class="blackBack">
	<div class="container">
		<h1>WELCOME TO THE HAWK WALL</h5>
		<div class="now_registered">
<?php
			if(isset($_SESSION['register_msg']))
			{
				for($i = 0; $i < count($_SESSION['register_msg']); $i++)
				{
?>
					<p><?=$_SESSION['register_msg'][$i]?></p>
<?php
				}
				unset($_SESSION['register_msg']);
			}
?>
		</div>
		<div class="errorBoxLog">
<?php
			if(isset($_SESSION['password']))
			{
				unset($_SESSION['password']);
			}
			if(isset($_SESSION['email']))
			{
				unset($_SESSION['email']);
			}
			if(isset($_SESSION['error_msg2']))
			{
				for($i = 0; $i < count($_SESSION['error_msg2']); $i++)
				{
?>
					<p class="errorText"><?=$_SESSION['error_msg2'][$i]?></p>
<?php
				}
				unset($_SESSION['error_msg2']);
			}
?>
		</div><!--errorBox-->

		<div class="loginBox">

			<h3>Returning User?</h3>
			<h3>Log In Now And Start Talking!!!</h3>
			<form method="POST" action="process.php">
				<input type="hidden" name="hidden_login">
				<label>Email Address: <input type="email" name="email"></label>
				<label>Password: <input type="password" name="password"></label>
				<input type="submit" name="login_submit" value="" class="btn">
			</form>
		</div><!--loginBox-->

		<div class="spacer"></div>

		<div class="errorBoxReg">
<?php 	
		//---------------Unset password and confirm if registration/login failed--------------//	
			if(isset($_SESSION['password']))
				{
					unset($_SESSION['password']);
				}
				if(isset($_SESSION['confirm']))
				{
					unset($_SESSION['confirm']);
				}
			if(isset($_SESSION['error_msg']) && $_SESSION['error_msg'] > 0)
			{
				for($i = 0; $i < count($_SESSION['error_msg']); $i++)
				{
?>
					<p class="errorText"><?=$_SESSION['error_msg'][$i]?></p>
<?php
				}
				unset($_SESSION['error_msg']);
			}
?>
		</div><!--errorBoxReg-->	
		
		<div class="registerBox">
			<h3> Not A User? Register Today!</h3>
			<p class="hawksSpace"></p>
			<form method="POST" action="process.php">
				<input type="hidden" name="hidden_register">
				<label>Email Address: <input type="email" name="email"
<?php
		//---------------------Place entered data back in fields------------------//

						if(isset($_SESSION['email']))
						{
?>
							value="<?=$_SESSION['email']?>"
<?php							
							unset($_SESSION['email']);
						}
?>
					></label>
				<label>First Name: <input type="text" name="first_name"
<?php
						if(isset($_SESSION['first_name']))
						{
?>
							value="<?=$_SESSION['first_name']?>"
<?php							
							unset($_SESSION['first_name']);
						}
?>
					>
				<label>Last Name: <input type="text" name="last_name"
<?php
						if(isset($_SESSION['last_name']))
						{
?>
							value="<?=$_SESSION['last_name']?>"
<?php							
							unset($_SESSION['last_name']);
						}
?>
					>
				<label>Password: <input type="password" name="password">
				<label>Confirm Password: <input type="password" name="confirm">
				<input type="submit" name="register_submit" value="" class="btn">
			</form>
		</div><!--registerBox-->
	</div><!--container-->
</body>
</html>