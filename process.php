<?php
SESSION_START();
require_once("new-connection.php");

// $query = "SELECT users.first_name, users.last_name, messages.created_at, messages.content, messages.id, messages.user_id, comments.id, comments.user_id, comments.message_id FROM users LEFT JOIN messages ON users.id = messages.user_id LEFT JOIN comments ON messages.id = comments.message_id LEFT JOIN users AS authors ON comments.user_id = authors.id ORDER BY messages.created_at DESC ORDER BY comments.created_at DESC";
$query = "SELECT users.first_name, users.last_name, messages.created_at, messages.content, messages.id AS msgID, messages.user_id, comments.id, comments.user_id, comments.message_id FROM users INNER JOIN messages ON users.id = messages.user_id LEFT JOIN comments ON messages.id = comments.message_id GROUP BY messages.id ORDER BY messages.created_at DESC";
$history = fetch_all($query);

$query2 = "SELECT comments.content, comments.message_id, comments.user_id, comments.created_at ,users.first_name, users.last_name FROM comments LEFT JOIN users ON comments.user_id = users.id";
$history2 = fetch_all($query2);

// die(var_dump($history2));





//---------------------BEGIN: LOG IN ---------------------//

if(isset($_POST['hidden_login']))
{
	if(isset($_SESSION['error_msg']))
	{
		unset($_SESSION['error_msg']);
	}
	if(isset($_SESSION['error_msg2']))
	{
		unset($_SESSION['error_msg2']);
	}

	//--------- Check email entered -------------//

	if(empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
	{
		$_SESSION['error_msg2'][] = "PLEASE ENTER A VALID EMAIL";
	}

	//--------- Check password entered -----------//

	if(empty($_POST['password']))
	{
		$_SESSION['error_msg2'][] = "PLEASE ENTER A VALID PASSWORD";
	}

	//---------- Errors? send back to log in -------//

	if(isset($_SESSION['error_msg2']) && $_SESSION['error_msg2'] > 0)
	{
		header("Location: index.wall.php");
	}
	else

	//--------- Compare Request/Database for login ---------//
	{
		$email = escape_this_string($_POST['email']);
		$password = escape_this_string($_POST['password']);
		$query = "SELECT email, password, id, first_name, last_name FROM users WHERE email='$email' && password='$password'";
		$credentials = fetch_record($query);
		$_SESSION['first_name'] = escape_this_string($credentials['first_name']);
		$_SESSION['last_name'] = escape_this_string($credentials['last_name']);
		$_SESSION['active_id'] = $credentials['id'];
		if(empty($credentials))
		{
			$_SESSION['error_msg2'][] = "Invalid Login Credentials";
			$_SESSION['error_msg2'][] = "Please Check Email/Password Combo";
			header("Location: index.wall.php");
		}
		else
		{
			header("Location: hawk_wall.php");
		}
	}
}


//---------------------BEGIN: REGISTRATION ---------------------//

if(isset($_POST['hidden_register']))
{
	//------ Reset error messages -------//

	if(isset($_SESSION['error_msg']))
	{
		die("here");
		unset($_SESSION['error_msg']);
	}

	//------- Check email entered -------//

	if(!empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
	{
		$_SESSION['email'] = $_POST['email'];
	}
	else
	{
		$_SESSION['error_msg'][] = "PLEASE ENTER A VALID EMAIL";
	}

	//------- Check first name entered ------//

	if(!empty($_POST['first_name']))
	{
		$_SESSION['first_name'] = $_POST['first_name'];
	}
	else
	{
		$_SESSION['error_msg'][] = "PLEASE ENTER YOUR FIRST NAME";
	}

	//------ Check last name entered ------//

	if(!empty($_POST['last_name']))
	{
		$_SESSION['last_name'] = $_POST['last_name'];
	}
	else
	{
		$_SESSION['error_msg'][] = "PLEASE ENTER YOUR LAST NAME";
	}

	//------- Check password and confirmation entered and matching ------//

	if(!empty($_POST['password']) && !empty($_POST['confirm']))
	{
		if($_POST['password'] == $_POST['confirm'])
		{
			$_SESSION['password'] = $_POST['password'];
		}
		else
		{
			$_SESSION['error_msg'][] = "Password/Confirmation Don't match";
		}
	}
	if(empty($_POST['password']))
	{
		$_SESSION['error_msg'][] = "PLEASE ENTER A PASSWORD";
	}
	if(empty($_POST['confirm']))
	{
		$_SESSION['error_msg'][] = "PLEASE CONFIRM YOUR PASSWORD";
	}

	//------- Check for error messages and direct ------//

	if(isset($_SESSION['error_msg']) && $_SESSION['error_msg'] > 0)
	{
		header("Location: index.wall.php");
		exit();
	}
	else

	//------- Register New User To Database -------//
	
	{
		$email = escape_this_string($_SESSION['email']);
		$firstname = escape_this_string($_SESSION['first_name']);
		$lastname = escape_this_string($_SESSION['last_name']);
		$password = escape_this_string($_SESSION['password']);
		$query = "INSERT INTO users (first_name, last_name, email, password, created_at) 
				  VALUES ('$firstname', '$lastname', '$email', '$password', NOW())";
		run_mysql_query($query);
		$_SESSION['register_msg'][] = "You're now a 12!!!";
		$_SESSION['register_msg'][] = "Welcome To The Hawks Nest!";
		header("Location: index.wall.php");
		exit();
	}	

	// var_dump($_POST);
	// die("there");
}

//---------------------------------Send Message To Database----------------//

	//-----Check message has content-------//

	if(isset($_POST['hidden_message']))
	{
		if(empty($_POST['message']))
		{
			$_SESSION['null_message'][] = "What!? NO LOVE!!??";
			$_SESSION['null_message'][] = "Can Not Post A Blank Message!";
			header("Location: hawk_wall.php");
		}
		else
		{
			$userID = escape_this_string($_SESSION['active_id']);
			$message = escape_this_string($_POST['message']);
			$query = "INSERT INTO messages (content, user_id, created_at) VALUES ('$message', '$userID', NOW())";
			run_mysql_query($query);
			header("Location: hawk_wall.php");
		}
	}

//-----------------------------Send Comment To Database-----------------------//

	//------Check comment has content------//

	if(isset($_POST['hidden_comment']))
	{
		if(empty($_POST['comment_text']))
		{
			$_SESSION['null_message2'][] = "Can Not Post Blank Comment";
			header("Location: hawk_wall.php");
		}
		else
		{
			$hiddencomment = escape_this_string($_POST['hidden_comment']);
			$userID = escape_this_string($_SESSION['active_id']);
			$comment = escape_this_string($_POST['comment_text']);
			$query = "INSERT INTO comments (content, created_at, user_id, message_id) VALUES ('$comment', NOW(), '$userID', '$hiddencomment')";
			run_mysql_query($query);
			header("Location: hawk_wall.php");
		}
	}

//----------------------------------Log Out------------------------------------//

	if(isset($_POST['logout']))
	{
		// die("here");
		unset($firstname);
		unset($lastname);
		unset($email);
		unset($password);
		header("Location: index.wall.php");
	}

?>

